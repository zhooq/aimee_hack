var querystring = require('querystring');
var http = require('https');
var fs = require('fs');
const uuidv4 = require('uuid/v4');
parseString = require('xml2js').parseString;

module.exports = {
	recognize : function(file, callback) {
		var speech = fs.readFile(file, function(err, data) {
      var uuid = uuidv4().replace(/-/g, '');

			var post_options = {
				host: 'asr.yandex.net',
				port: '443',
				path: '/asr_xml?uuid='+uuid+'&key=1cfb215e-5e35-48aa-aee2-76a9d0b54ee6&topic=queries',
				method: 'POST',
				headers: {
					'Content-Type': 'audio/ogg;codecs=opus',
					'Content-Length': data.length
				}
			};

			var post_req = http.request(post_options, function(res) {
				res.setEncoding('utf8');
				res.on('data', function (chunk) {
          parseString(chunk, function (err, result) {
            results_array = [];

            if (result.recognitionResults.$.success == "1") {
              for (x of result.recognitionResults.variant) {
                results_array.push(x._);
              }
            }

            callback(results_array);
          });

					
				});
			});

			post_req.write(data);
			post_req.end();
		});
	}
}