import requests
import pandas as pd

class AimeeApiSDK(object):
    
    def __init__(self, ip, autoanswer_threshold=0.85):
        self.ip = ip
        self.autoanswer_threshold = autoanswer_threshold
    
    def send_data(self, question, answer, operator, tag=""):
        """
        Add a new question-answer pair
        """
        response = requests.post("http://{}/data/".format(self.ip), 
                                 json={
                "answer": str(answer),
                "question": str(question),
                "operator": str(operator),
                "tag": str(tag),
            })
        return response.json(), response
    
    def get_suggestions(self, key, count=None, tags=""):
        """
        Get suggested answers for a given question
        """
        response = requests.get(
            "http://{}/answer/".format(self.ip), 
            params=(
                ('key', key), 
                ('count', count or 5),
                ('tags', tags)))
        
        if response.ok:
            suggestions = response.json()
            df = pd.DataFrame(suggestions)[['answer', 'answerId', 'autoanswer', 'score']]
            return list(df.T.to_dict().values())
        
        else:
            return None
    
    def get_answer(self, key, tags=""):
        """
        Get answer. Bot will answer only in case if it is has autoanswer for a given question.
        Consider modifying bot configs if you want to change bot's restrictions for autoanswer
        """
        answers = self.get_suggestions(key, 5)
        auto_answer = None
        if answers is not None:
            for answer in answers:
                if answer['autoanswer'] and answer['score'] >= self.autoanswer_threshold:
                    auto_answer = answer['answer']
                    break
                    
        return auto_answer     
    
    def train(self):
        """
        Update long-term memory
        """
        response = requests.post("http://{}/ai_training/".format(self.ip))
        return response.json(), response
    
    def get_last_train_info(self):
        """
        Get last train info 
        """
        response = requests.get("http://{}/ai_training/".format(self.ip))
        if response.ok:
            return response.json()
        else:
            return None
        
    def get_config(self):
        """
        Get current bot configuration
        """
        response = requests.get("http://{}/ai_config/".format(self.ip))
        if response.ok:
            return response.json()
        else:
            return None
    
    def set_config(self, config):
        """
        Set bot configuration
        """
        response = requests.post("http://{}/ai_config/".format(self.ip), params=config)
        return response.ok
    
    def delete_answer_by_id(self, answerId, question=None, answer=None):
        """
        Delete answer by Id
        """
        params = {'answerId': answerId}
        if question is not None:
            params['question'] = question
            params['answer'] = answer
            
        response = requests.post(
            "http://{}/deleted_answers/".format(self.ip),
            params=params
        )
        
        return response.ok
    
    def reset(self):
        """
        Clears data from db and memory.
        """
        response = requests.post(
            "http://{}/reset/".format(self.ip)
        )
        return response