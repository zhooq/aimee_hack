const TelegramBot = require('node-telegram-bot-api');
const speech = require('@google-cloud/speech');
const fs = require('fs');
const express = require('express');
const app = express();

const request = require("request");

const client = new speech.SpeechClient();

const config = {
  encoding: 'OGG_OPUS',
  sampleRateHertz: 16000,
  languageCode: 'ru-RU',
  model: 'default',
  enableAutomaticPunctuation: true
};

const token = '578233929:AAGx1Wh5TBUJ5e_7184o9xj99na7PdIzo90';

let globalData = [];

const bot = new TelegramBot(token, {polling: true});

// Trigger on text message and proceed message
bot.on('text', (msg) => {
  console.log(msg.date + ':' + msg.text)
  proceedText(msg.text, msg.date)
});


// Trigger on voice then recognize text and proceed message
bot.on('voice', (msg) => {

  bot.downloadFile(msg.voice.file_id, './').then((resp) => {
    const file = fs.readFileSync(resp);
    const audioBytes = file.toString('base64');
    const audio = {
      content: audioBytes,
    };
    const request = {
      audio: audio,
      config: config,
    };

    client
      .recognize(request)
      .then(data => {
        const response = data[0];
        const transcription = response.results
          .map(result => result.alternatives[0].transcript)
          .join('\n');
        console.log(msg.date + ':' + transcription)
        proceedText(transcription, msg.date)
      })
      .catch(err => {
        console.error('ERROR:', err);
      });
  })
});

// Request to the Aimee and try to get answer
function proceedText(question, time) {
  let options = {
    method: 'GET',
    url: 'http://40.91.222.177:4560/answer/',
    qs: {key: question},
    headers:
      {'cache-control': 'no-cache'}
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    try {
      if (typeof JSON.parse(body)[0] !== "undefined") {
        globalData.push([time, JSON.parse(body)[0].answer.toLowerCase()])
      }
    } catch (e) {
      console.log(e)
    }
  });
}


// Return statistics for web application
app.get('/stat', function (req, res) {
  res.send(JSON.stringify(globalData))
});

app.use(express.static('../front/dist'))

const cache = []

app.get('/timeline', async (req, res, next) => {
  try {
    res.send(JSON.stringify(globalData))//[[1523667193, 'group1'], [1523667194, 'group2']]))
    // cache.push([ Date.now(), randTopic()])
  } catch (err) {
    console.log(err)
    res.send({error: 'Request failed'})
  }
})


app.listen(3000, () => console.log('Listening on port 3000!'));