const express = require('express')
const app = express()
const http = require('http').Server(app)
const io = require('socket.io')(http)

const topics = ['group1', 'group2', 'group3', 'group4', 'group5', 'group6']
const randTopic = () => {
  return topics[Math.floor(Math.random() * (6))]
}

// io.on('connection', client => {
//   setInterval(() => {
//     io.emit('upd', {series: [[ new Date().getUTCMilliseconds(), randTopic()]]})
//   }, 10000)
// })

app.use(express.static('../front/dist'))

const cache = []

app.get('/timeline', async (req, res, next) => {
  try {
    res.send(JSON.stringify(cache))//[[1523667193, 'group1'], [1523667194, 'group2']]))
    cache.push([ Date.now(), randTopic()])
  } catch (err) {
    console.log(err)
    res.send({error: 'Request failed'})
  }
})

http.listen(3001, () => console.log('listening on port: 3001'))