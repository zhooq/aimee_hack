import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

Vue.use(Vuex)

const topics = {
  group1: 'Текучка',
  group2: 'Рабочий процесс',
  group3: 'Конфликты',
  group4: 'Персонал',
  group5: 'Банкоматы',
  group6: 'Интернет банкинг'
}
const colors = {
  group1: '#7775f8',
  group2: '#f87979',
  group3: '#37f883',
  group4: '#f8de33',
  group5: '#c582f8',
  group6: '#c5f8bd'
}

export default new Vuex.Store({
  state: {
    timeline: [],
    topics,
    colors,
    CurrentCustomerIssues: {
      labels: [topics['group4'], topics['group5'], topics['group6']],
      datasets: [
        {
          backgroundColor: [colors['group4'], colors['group5'], colors['group6']],
          data: [0, 0, 0]
        }
      ]
    },
    CurrentStaffIssues: {
      labels: [topics['group1'], topics['group2'], topics['group3']],
      datasets: [
        {
          backgroundColor: [colors['group1'], colors['group2'], colors['group3']],
          data: [0, 0, 0]
        }
      ]
    },
    CustomerIssues: {
      datasets: [
        {
          label: topics['group4'],
          borderColor: colors['group4'],
          data: []
        },
        {
          label: topics['group5'],
          borderColor: colors['group5'],
          data: []
        },
        {
          label: topics['group6'],
          borderColor: colors['group6'],
          data: []
        }
      ]
    },
    StaffIssues: {
      datasets: [
        {
          label: topics['group1'],
          borderColor: colors['group1'],
          data: []
        },
        {
          label: topics['group2'],
          borderColor: colors['group2'],
          data: []
        },
        {
          label: topics['group3'],
          borderColor: colors['group3'],
          data: []
        }
      ]
    }
  },
  actions,
  mutations,
  getters
})
