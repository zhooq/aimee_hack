export default {
  // TIMELINE (state, timeline) {
  //   state.timeline = timeline
  // },
  // UPD_TIMELINE (state, upd) {
  //   state.timeline.push(...upd)
  // },
  CURRENT_CUSTOMER_ISSUES (state, issues) {
    state.CurrentCustomerIssues = issues
  },
  CUSTOMER_ISSUES (state, issues) {
    state.CustomerIssues = issues
  },
  CURRENT_STAFF_ISSUES (state, issues) {
    state.CurrentStaffIssues = issues
  },
  STAFF_ISSUES (state, issues) {
    state.StaffIssues = issues
  }
}
