import _ from 'lodash'
import Vue from 'vue'

const UPDATE = (ctx, input) => {
  const timeline = new Map()
  // const input = ctx.state.timeline
  _.each(input, el => timeline.set(el[0], el[1]))
  const keys = _.map(input, el => el[0])
  const max = _.max(keys)
  const min = _.min(keys)
  const avr = Math.floor((max + min) / 2)
  const stat = _.reduce(keys, (collector, el) => {
    const hist = collector[timeline.get(el)]
    if ( Math.abs(el - max) < Math.abs(el - avr) && Math.abs(el - max) < Math.abs(el - min) ) {
      hist[2] += 1
    } else if ( Math.abs(el - avr) < Math.abs(el - min) ) {
      hist[1] += 1
    } else {
      hist[0] += 1
    }
    return collector
  },{
    group1: [0, 0, 0],
    group2: [0, 0, 0],
    group3: [0, 0, 0],
    group4: [0, 0, 0],
    group5: [0, 0, 0],
    group6: [0, 0, 0]
  })
  //   {
  //   group1: [0, 0, 0],
  //   group2: [0, 0, 1],
  //   group3: [0, 0, 3],
  //   group4: [3, 40, 6],
  //   group5: [15, 20, 7],
  //   group6: [4, 2, 9]
  // })

  const labels = [new Date(min).toLocaleTimeString(), new Date(avr).toLocaleTimeString(), new Date(max).toLocaleTimeString()]
  const topics = {
    group1: 'Текучка',
    group2: 'Рабочий процесс',
    group3: 'Конфликты',
    group4: 'Персонал',
    group5: 'Банкоматы',
    group6: 'Интернет банкинг'
  }
  const colors = {
    group1: '#7775f8',
    group2: '#f87979',
    group3: '#37f883',
    group4: '#f8de33',
    group5: '#c582f8',
    group6: '#c5f8bd'
  }
  const currentCustomer = {
    labels: [topics['group4'], topics['group5'], topics['group6']],
    datasets: [
      {
        backgroundColor: [colors['group4'], colors['group5'], colors['group6']],
        data: [stat['group4'][2], stat['group5'][2], stat['group6'][2]]
      }
    ]
  }

  const customerIssues = {
    labels,
    datasets: [
      {
        label: topics['group4'],
        borderColor: colors['group4'],
        data: stat['group4']
      },
      {
        label: topics['group5'],
        borderColor: colors['group5'],
        data: stat['group5']
      },
      {
        label: topics['group6'],
        borderColor: colors['group6'],
        data: stat['group6']
      }
    ]
  }

  const currentStaff = {
    labels: [topics['group1'], topics['group2'], topics['group3']],
    datasets: [
      {
        backgroundColor: [colors['group1'], colors['group2'], colors['group3']],
        data: [stat['group1'][2], stat['group2'][2], stat['group3'][2]]
      }
    ]
  }

  const staffIssues = {
    labels,
    datasets: [
      {
        label: topics['group1'],
        borderColor: colors['group1'],
        data: stat['group1']
      },
      {
        label: topics['group2'],
        borderColor: colors['group2'],
        data: stat['group2']
      },
      {
        label: topics['group3'],
        borderColor: colors['group3'],
        data: stat['group3']
      }
    ]
  }

  ctx.commit('CURRENT_CUSTOMER_ISSUES', currentCustomer)
  ctx.commit('CUSTOMER_ISSUES', customerIssues)
  ctx.commit('CURRENT_STAFF_ISSUES', currentStaff)
  ctx.commit('STAFF_ISSUES', staffIssues)
}
export default {
  async FETCH_ALL (ctx) {
    // const input = [[1523667193, 'group1'], [1523667194, 'group2']]
    // UPDATE(ctx, input)

    try {
      const resp = await Vue.prototype.$http.get('/timeline')
      const input = resp.data
      console.log(resp.data)
      // ctx.commit('TIMELINE', input)
      UPDATE(ctx, input)
    } catch (err) {
      console.log(err)
    }
  },
  async UPD (ctx) {
    try {
      UPDATE(ctx)
    } catch (err) {
      console.log(err)
    }
  }
}
